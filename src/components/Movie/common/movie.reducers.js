import { keys } from './movie.actions';
import { createReducer } from '../../../common/redux.helpers';

const movieReducer = createReducer({ isVisible: false, movieId: undefined }, {
  [keys.SHOW_MOVIE]: (state, action) => ({
    isVisible: true,
    movieId: action.movieId
  }),
  [keys.HIDE_MOVIE]: (state, action) => ({
    ...state,
    isVisible: false
  })
});

export default movieReducer;