import React from 'react';
import _ from 'lodash';
import { Spin } from 'antd';
import 'antd/dist/antd.css';

const Loader = ({isLoading, children}) => {
  if (isLoading) {
    return (
      <Spin size="large"/>
    );
  }

  return children ? children : null;
}

export default Loader;