import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Card, Heading } from 'react-bulma-components/full'
import { showMovie } from './common/movie.actions'

class MovieCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {

    const { movie, showMovie } = this.props;

    return (
      <Card onClick={() => showMovie(movie.id)}>
        <Card.Image src={movie.poster_path} alt={movie.title}></Card.Image>
        <Card.Content>
          <Heading size="5">
            {movie.title}
          </Heading>
        </Card.Content>
      </Card>
    );

  }

}

export default connect(
  () => ({}),
  { showMovie }
)(MovieCard);