export const createReducer = (initialState = {}, actionHandlerKeyFuncs = {}) => {
  return (state = initialState, action) => {
    const actionHandler = actionHandlerKeyFuncs[action.type];
    return actionHandler ? actionHandler(state, action) : state;
  }
};

//Basic action creator
const createAction = (type, actionProps) => {
  return {
    type,
    ...actionProps
  };
}

// Async action creator
export const createAsyncActionCreator = (actionType, asyncRequestFn, requestParams) => {
  return (dispatch) => {
    dispatch(createAction(`${actionType}_START`, { request: requestParams }));
    return asyncRequestFn(requestParams)
      .then(response => {
        response.json()
          .then(json => dispatch(createAction(`${actionType}_SUCCESS`, { response: json })))
          .catch(error => dispatch(createAction(`${actionType}_ERROR`, { error })));
      });

  };
}

//Setting initial async state
const initialAsyncState = { isLoading: false, response: null, request: null };

//Handling state changes
export const createAsyncReducer = (actionType, actionHandlerKeyFuncs = {}, initialState = initialAsyncState) => {

  const startReducerFn = (state, action) => ({
    ...state,
    isLoading: true,
    request: action.request
  });

  const successReducerFn = (state, action) => ({
    ...state,
    isLoading: false,
    response: action.response
  });

  const errorReducerFn = (state, action) => ({
    ...state,
    isLoading: false,
    error: action.error
  });

  return createReducer(
    initialState,
    {
      [`${actionType}_START`]: startReducerFn,
      [`${actionType}_SUCCESS`]: successReducerFn,
      [`${actionType}_ERROR`]: errorReducerFn,
      ...actionHandlerKeyFuncs
    }
  );  
  
}