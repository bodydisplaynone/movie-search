import React, { Component } from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import { hideMovie } from '../Movie/common/movie.actions';
import { getMovieDetails } from '../../containers/Movies/common/movies.actions';
import * as movieHelpers from '../../containers/Movies/common/movies.helpers';
import { Modal } from 'react-bulma-components/full'


class Movie extends Component {

  componentWillReceiveProps(nextProps) {
    if (nextProps.movieId && this.props.movieId !== nextProps.movieId) {
      nextProps.getMovieDetails(nextProps.movieId);
    }
  }

  render() {
    const {isVisible, hideMovie} = this.props;
    const movie = [];
    const genres = (movie && movie.genres) ? movie.genres.map(genre => genre.name).join(', ') : '';

    return (
      <Modal
      show={isVisible}
      onRequestClose={hideMovie}
      >
          <div>
            <h1>{movie.title}</h1>
            <h5>{genres}</h5>
            <p>{movie.overview}</p>
            <p>Popularity: {movie.popularity}</p>
            <p>Budget: ${movie.budget}</p>
          </div>
    </Modal>
    );
  }
}
export default connect(
  (state) => ({
    isVisible: _.get(state, 'Movies.Movie.isVisible', false),
    movieId: _.get(state, 'Movies.Movie.movieId'),
    movie: _.get(state, 'Movies.movieDetails.response', {}),
  }),
  { hideMovie, getMovieDetails }
)(Movie);