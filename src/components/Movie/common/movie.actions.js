export const keys = {
    'SHOW_MOVIE': 'SHOW_MOVIE',
    'HIDE_MOVIE': 'HIDE_MOVIE',
  }
  
  export const showMovie = (movieId) => {
    return {
      type: keys.SHOW_MOVIE,
      movieId
    };
  }
  
  export const hideMovie = () => {
    return {
      type: keys.HIDE_MOVIE
    };
  }