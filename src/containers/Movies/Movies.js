import React, { Component } from 'react';
import { Container, Columns, Form } from 'react-bulma-components/full';
import { connect } from 'react-redux';
import * as infiniteScroll from '../../common/infiniteScroll';
import * as movieActions from './common/movies.actions';
import * as movieHelpers from './common/movies.helpers';
import MovieList from '../../components/Movie/MovieList';
import Movie from '../../components/Movie/Movie'

class Movies extends Component {

  constructor(props) {
    super(props);

    this.state = {
      currentPage: 1,
      searchQuery: '',
    };

    this.handleScroll = this.handleScroll.bind(this);
    this.handleinput = this.handleInput.bind(this);
}

  componentDidMount() {
    window.onscroll = this.handleScroll;
    this.props.getTopMovies(this.state.currentPage);
  }

  componentDidUpdate() {
      this.props.searchMovies(this.state.searchQuery, this.state.currentPage);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
}

handleScroll() {
  const {topMovies} = this.props;
  if (!topMovies.isLoading) {
    let percentageScrolled = infiniteScroll.getScrollDownPercentage(window);
    if (percentageScrolled > .8) {
      const nextPage = this.state.currentPage + 1;
      this.props.getTopMovies(nextPage);
      this.setState({currentPage: nextPage});
    }
  }
}

handleInput(event) {
  this.setState({
    searchQuery: event.target.value,

  });
}

  render() {
    
    //const { topMovies } = this.props;
    //const movies = movieHelpers.getMoviesList(topMovies.response);

    const { searchMovies, topMovies } = this.props;
    const searched = movieHelpers.getMoviesList(searchMovies.response);
    const top = movieHelpers.getMoviesList(topMovies.response);

    return (
        <Container>

            <Columns.Column>
            <Form.Input
            placeholder="Search for a movie"
            value={this.state.searchQuery}
            onChange={this.handleinput}
            >
            </Form.Input>
            </Columns.Column>

            <Columns>
            <MovieList
             movies={searched}
             isLoading={searchMovies.isLoading}
             />
             <MovieList
             movies={top}
             isLoading={topMovies.isLoading}
             />
            </Columns>
            <Movie />
        </Container>
    );
  }
}

export default connect(
  (state) => ({
    topMovies: state.Movies.topMovies,
    searchMovies: state.Movies.searchMovies
  }),
  { ...movieActions }
)(Movies);