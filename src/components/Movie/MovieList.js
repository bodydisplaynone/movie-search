import React from 'react';
import { Columns } from 'react-bulma-components/full';
import MovieCard from './MovieCard';
import Loader from '../../common/loader';

const MovieList = ({movies, isLoading}) => {

  console.log(movies);
    
  const movieCols = movies ? movies.map(movie => (
    <Columns.Column size={2} key={movie.id}>
      <MovieCard movie={movie} />
    </Columns.Column>
  )) : null;
  
  return (
    <>
      {movieCols}
      <Loader isLoading={isLoading} />
    </>
  );
}

export default MovieList;