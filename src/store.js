import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunkMiddlware from 'redux-thunk';
import { createLogger } from 'redux-logger'
import moviesReducer from './containers/Movies/common/movies.reducers';

const rootReducer = combineReducers({
  Movies: moviesReducer
});

const loggerMiddleware = createLogger();

const store = createStore(
  rootReducer,
  undefined,
  compose(
    applyMiddleware(
      thunkMiddlware,
      loggerMiddleware
    )
  )
);

export default store;